FROM debian:bullseye-slim

RUN export DEBIAN_FRONTEND=noninteractive &&\
    # Update
    apt update && apt-get upgrade -y && apt-get -y install \
    # App
    supervisor cpulimit htop procps nano &&\
    # Remove useless
    apt-get autoclean -y && apt-get autoremove -y

WORKDIR /root

# Copy Server config
COPY src/config_dump/ ./server

# Custom Lib
# By default, the `ld` linker/loader will look in /lib, /usr/lib, and the directories listed in /etc/ld.so.conf. However, if the LD_LIBRARY_PATH environment variable is set, the `ld` linker/loader will look in the directories listed in this environment variable first. This allows for users to specify their own, different versions of shared libraries that the programs they are running will use.
ENV LD_LIBRARY_PATH='/root/server/lib'
ENV ASAN_OPTIONS='include_if_exists=/root/server/lib/asan.options'

# Copy config file
COPY src/config_app/supervisord.conf /etc/supervisor/supervisord.conf
COPY src/config_app/app.sh           ./app.sh
COPY src/config_app/prefix-output.sh ./prefix-output.sh

ENTRYPOINT ["sh", "app.sh"]