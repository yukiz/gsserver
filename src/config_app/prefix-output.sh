#!/bin/bash

help() {
  echo "Usage: gio
               [ -r | --run ]
               [ -ip | --address ]
               [ -l | --limit ]
               [ -h | --help  ]"
  exit 2
}

SHORT=r:,ip:,h:l
LONG=run:,address:,limit:,help
OPTS=$(getopt -a -n gio --options $SHORT --longoptions $LONG -- "$@")

VALID_ARGUMENTS=$# # Returns the count of arguments that are in short or long options

if [ "$VALID_ARGUMENTS" -eq 0 ]; then
  help
fi

eval set -- "$OPTS"

while :; do
  case "$1" in
  -r | --run)
    run="$2"
    shift 2
    ;;
  -ip | --address)
    address="$2"
    shift 2
    ;;
  -l | --limit)
    limit="$2"
    shift 2
    ;;
  -h | --help)
    help
    ;;
  --)
    shift
    break
    ;;
  *)
    echo "Unexpected option: $1"
    help
    ;;
  esac
done

# Get prefix from SUPERVISOR_PROCESS_NAME environment variable
printf -v PREFIX "%-10.10s" "${SUPERVISOR_PROCESS_NAME}"

# Prefix stdout and stderr
exec 1> >(perl -ne '$| = 1; print "'"${PREFIX}"' | $_"' >&1)
exec 2> >(perl -ne '$| = 1; print "'"${PREFIX}"' | $_"' >&2)

echo "Run: $run"
echo "Address: $address"
echo "Limit: $limit"

cd server/$run/

if [ -f $run ]; then
  if [ "$limit" -gt 0 ]; then
    ./"$run" -i "$address" &
    attack_me=$!
    echo "Set pid: $attack_me at $run"
    cpulimit -l "$limit" -p "$attack_me"
  else
    echo "runs without limit: $run"
    ./"$run" -i "$address"    
  fi
else
  exit 1
fi
