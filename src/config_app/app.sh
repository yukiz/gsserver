#!/bin/sh

ldconfig
printenv

if [ -z "$host_api" ]; then
  host_api="ps.yuuki.me"
fi

if [ -z "$region" ]; then
  region="dev_gio"
fi

# ip local
if [ -z "$local_ip" ]; then
  local_ip=$(hostname -i)
fi

# ip public
if [ -z "$remote_ip" ]; then
  remote_ip=$(hostname -i)
fi

# mysql
if [ -z "$ip_mysql" ]; then
  ip_mysql=$(hostname -i)
fi
if [ -z "$port_mysql" ]; then
  port_mysql="3306"
fi

# redis
if [ -z "$ip_redis" ]; then
  ip_redis=$(hostname -i)
fi
if [ -z "$port_redis" ]; then
  port_redis="6379"
fi

# password & user datebase
if [ -z "$pass_db" ]; then
  pass_db="GenshinImpactOffline2022"
fi
if [ -z "$user_db" ]; then
  user_db="work"
fi

# Max connected number
if [ -z "$MaxConnNum" ]; then
  MaxConnNum=100000
fi

if [ -z "$reload_config" ]; then
  reload_config=-1 # set -1 for off, if set 300 it will check every 5 minutes. This is useful for development but very bad for product because it reduces performance. use this if you want to auto reload config without restarting
fi

if [ -z "$ping" ]; then
  ping=300 # ping or heartbeat, this will send a ping every 5 minutes to people who are online and if the config on the client is set to true, it is useful for kicking/giving a warning if the player plays too long
fi

# 20041 GAME
if [ -z "$port_game" ]; then
  port_game="20041"
fi
echo "Port Game: $port_game"
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_GAME_PORT/$port_game/g" {} +

# 20011 GM API
if [ -z "$port_gm" ]; then
  port_gm="20011"
fi
if [ -z "$key_gm" ]; then
  key_gm="" # secure your miuip
fi
echo "Port GM: $port_gm"
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_GM_PORT/$port_gm/g" {} +

echo "Local IP: $local_ip"
echo "Public IP: $remote_ip"

echo "MYSQL IP: $ip_mysql"
echo "MYSQL PORT: $port_mysql"

echo "REDIS IP: $ip_redis"
echo "REDIS PORT: $port_redis"

echo "DB PASSWORD: $pass_db"
echo "DB USER: $user_db"

echo "Max Connected Number: $MaxConnNum"
echo "Config Reload: $reload_config sec"
echo "Ping: $ping"
echo "Host API: $host_api"
echo "Region: $region"
echo "KEY GM: $key_gm"

find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_MAXCONNNUM/$MaxConnNum/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_CONFIGRELOAD/$reload_config/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_REGION/$region/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_HOSTAPI/$host_api/g" {} +

find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_PASSWORD_DB/$pass_db/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_USER_DB/$user_db/g" {} +

find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_IP_MYSQL/$ip_mysql/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_PORT_MYSQL/$port_mysql/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_IP_REDIS/$ip_redis/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_PORT_REDIS/$port_redis/g" {} +

find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_DEVICE_IP/$local_ip/g" {} +
find . -name "*.xml" -not -path "./server/data/*" -exec sed -i "s/REPLACE_IT_TO_YOUR_ACCESS_IP/$remote_ip/g" {} +

find . -name "config.json" -exec sed -i "s/REPLACE_IT_TO_YOUR_ACCESS_IP/$remote_ip/g" {} +
find . -name "muipserver.xml" -exec sed -i "s/REPLACE_GMKEY/$key_gm/g" {} +
find . -name "gameserver.xml" -exec sed -i "s/REPLACE_PINGME/$ping/g" {} +

echo "Servers started running..."
supervisord