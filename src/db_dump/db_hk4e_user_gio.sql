/*
 YuukiPS
 Date: 18/02/2024
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_block_data_0
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_0`;
CREATE TABLE `t_block_data_0`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`, `block_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '场景block存档' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_0
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_1
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_1`;
CREATE TABLE `t_block_data_1`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_1
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_2
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_2`;
CREATE TABLE `t_block_data_2`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_2
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_3
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_3`;
CREATE TABLE `t_block_data_3`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_3
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_4
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_4`;
CREATE TABLE `t_block_data_4`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_4
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_5
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_5`;
CREATE TABLE `t_block_data_5`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_5
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_6
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_6`;
CREATE TABLE `t_block_data_6`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_6
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_7
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_7`;
CREATE TABLE `t_block_data_7`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_7
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_8
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_8`;
CREATE TABLE `t_block_data_8`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_8
-- ----------------------------

-- ----------------------------
-- Table structure for t_block_data_9
-- ----------------------------
DROP TABLE IF EXISTS `t_block_data_9`;
CREATE TABLE `t_block_data_9`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `data_version` int(11) NOT NULL,
  `bin_data` mediumblob NOT NULL,
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_block_data_9
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_0
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_0`;
CREATE TABLE `t_home_data_0`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '家园存档' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_0
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_1
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_1`;
CREATE TABLE `t_home_data_1`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_1
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_2
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_2`;
CREATE TABLE `t_home_data_2`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_2
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_3
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_3`;
CREATE TABLE `t_home_data_3`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_3
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_4
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_4`;
CREATE TABLE `t_home_data_4`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_4
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_5
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_5`;
CREATE TABLE `t_home_data_5`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_5
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_6
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_6`;
CREATE TABLE `t_home_data_6`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_6
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_7
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_7`;
CREATE TABLE `t_home_data_7`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_7
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_8
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_8`;
CREATE TABLE `t_home_data_8`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_8
-- ----------------------------

-- ----------------------------
-- Table structure for t_home_data_9
-- ----------------------------
DROP TABLE IF EXISTS `t_home_data_9`;
CREATE TABLE `t_home_data_9`  (
  `uid` int(11) NOT NULL DEFAULT 0,
  `bin_data` mediumblob NOT NULL,
  `data_version` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `block_end_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '封禁结束时间',
  `last_save_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预留字段2'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_home_data_9
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_0
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_0`;
CREATE TABLE `t_player_data_0` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_0
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_1
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_1`;
CREATE TABLE `t_player_data_1` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_1
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_2
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_2`;
CREATE TABLE `t_player_data_2` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_2
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_3
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_3`;
CREATE TABLE `t_player_data_3` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_3
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_4
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_4`;
CREATE TABLE `t_player_data_4` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_4
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_5
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_5`;
CREATE TABLE `t_player_data_5` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_5
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_6
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_6`;
CREATE TABLE `t_player_data_6` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_6
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_7
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_7`;
CREATE TABLE `t_player_data_7` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_7
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_8
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_8`;
CREATE TABLE `t_player_data_8` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_8
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_data_9
-- ----------------------------
DROP TABLE IF EXISTS `t_player_data_9`;
CREATE TABLE `t_player_data_9` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0,
  `nickname` varchar(128) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  `vip_point` int(10) unsigned NOT NULL DEFAULT 0,
  `json_data` varchar(512) NOT NULL DEFAULT '',
  `bin_data` mediumblob NOT NULL,
  `extra_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据',
  `data_version` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_list` varchar(128) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_save_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为polardb扩容后废弃数据',
  `reserved_1` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段1',
  `reserved_2` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '预留字段2',
  `before_login_bin_data` blob DEFAULT NULL COMMENT 'login前用的数据（对比extra_bin_data没有多写的问题）',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家核心数据包';

-- ----------------------------
-- Records of t_player_data_9
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_online_id_data
-- ----------------------------
DROP TABLE IF EXISTS `t_player_online_id_data`;
CREATE TABLE `t_player_online_id_data` (
  `online_id` varchar(64) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`online_id`) USING BTREE,
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='PS玩家online与uid映射';

-- ----------------------------
-- Records of t_player_online_id_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_psn_online_id_data
-- ----------------------------
DROP TABLE IF EXISTS `t_player_psn_online_id_data`;
CREATE TABLE `t_player_psn_online_id_data` (
  `psn_id` varchar(64) NOT NULL,
  `online_id` varchar(64) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`psn_id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `online_id` (`online_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='ps用户psn_id到online_id/uid映射';

-- ----------------------------
-- Records of t_player_psn_online_id_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_player_uid
-- ----------------------------
DROP TABLE IF EXISTS `t_player_uid`;
CREATE TABLE `t_player_uid` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_type` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '账号类型',
  `account_uid` varchar(128) NOT NULL DEFAULT '' COMMENT '绑定的账号UID',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `ext` varchar(512) NOT NULL DEFAULT '' COMMENT '自定义信息，Json格式',
  `tag` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'TAG，由MUIP设置',
  PRIMARY KEY (`uid`) USING BTREE,
  UNIQUE KEY `account_type_account_uid` (`account_type`,`account_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家身份信息表';

SET FOREIGN_KEY_CHECKS = 1;
