/*
 YuukiPS
 Date: 18/02/2024
*/

SET @url_api := 'ps.yuuki.me';

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_account_cancellation_config
-- ----------------------------
DROP TABLE IF EXISTS `t_account_cancellation_config`;
CREATE TABLE `t_account_cancellation_config` (
  `uid` int(11) unsigned NOT NULL COMMENT '游戏uid',
  `account_uid` bigint(20) unsigned NOT NULL COMMENT '通行证aid',
  `cancellation_time` varchar(50) NOT NULL COMMENT '账号注销时间',
  `create_timestamp` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='米哈游通行证注销名单';

-- ----------------------------
-- Records of t_account_cancellation_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_activity_data
-- ----------------------------
DROP TABLE IF EXISTS `t_activity_data`;
CREATE TABLE `t_activity_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(4) unsigned NOT NULL,
  `schedule_id` int(4) unsigned NOT NULL,
  `activity_type` int(4) unsigned NOT NULL COMMENT '活动类型，避免策划activity_id做新的活动',
  `bin_data` blob NOT NULL COMMENT '使用protobuf序列化后的二进制字段',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `activity_schedule_id` (`activity_id`,`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='全服活动存档数据';

-- ----------------------------
-- Records of t_activity_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_activity_schedule_config`;
CREATE TABLE `t_activity_schedule_config` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '排期ID',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `desc` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='活动排期表';

-- ----------------------------
-- Records of t_activity_schedule_config
-- ----------------------------
INSERT INTO `t_activity_schedule_config` VALUES (5023001, '2024-01-01 00:00:00', '2024-12-31 00:00:00', 'Windy');
INSERT INTO `t_activity_schedule_config` VALUES (5051001, '2024-01-01 00:00:00', '2024-12-31 00:00:00', 'Bantan Sango Case Files: The Warrior Dog; required to unlock Omni-Ubiquity Net');
INSERT INTO `t_activity_schedule_config` VALUES (5099001, '2024-01-01 00:00:00', '2024-12-31 00:00:00', 'Adventurer Trials');
INSERT INTO `t_activity_schedule_config` VALUES (5107001, '2024-01-01 00:00:00', '2024-12-31 00:00:00', 'Hypostatic Symphony: Dissonant Verse');
INSERT INTO `t_activity_schedule_config` VALUES (5108001, '2024-01-01 00:00:00', '2024-12-31 00:00:00', 'Outside the Canvas, Inside the Lens: Greenery Chapter');

-- ----------------------------
-- Table structure for t_announce_config
-- ----------------------------
DROP TABLE IF EXISTS `t_announce_config`;
CREATE TABLE `t_announce_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `center_system_text` varchar(200) NOT NULL DEFAULT '' COMMENT '中央系统提示文本',
  `count_down_text` varchar(200) NOT NULL DEFAULT '' COMMENT '倒计时提示文本',
  `dungeon_confirm_text` varchar(200) NOT NULL DEFAULT '' COMMENT '地下城确认框文本',
  `center_system_frequency` int(11) NOT NULL COMMENT '跑马灯频率',
  `count_down_frequency` int(11) NOT NULL COMMENT '倒计时频率',
  `channel_config_str` varchar(50) NOT NULL COMMENT '渠道配置',
  `is_center_system_last_5_every_minutes` tinyint(4) NOT NULL DEFAULT 1 COMMENT '跑马灯最后5分钟每分钟通知',
  `channel_id_list` varchar(50) NOT NULL COMMENT '渠道ID列表',
  `platform_type_list` varchar(50) NOT NULL COMMENT '客户端平台类型',
  `enable` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `server_version` varchar(64) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='预告功能配置表';

-- ----------------------------
-- Records of t_announce_config
-- ----------------------------
INSERT INTO `t_announce_config` VALUES (1, '2024-02-01 00:00:00', '2024-02-29 00:00:00', 'Welcome to YuukiPS! Our server is free for all, but maintaining it is costly. Consider sponsoring at ps.yuuki.me/sponsor to keep it running and make everyone happy.', 'Join our community discord.yuuki.me', '', 10, 60, '', 1, '0,1,2,3', '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14', 1, '11793813');

-- ----------------------------
-- Table structure for t_anti_offline_whitelist
-- ----------------------------
DROP TABLE IF EXISTS `t_anti_offline_whitelist`;
CREATE TABLE `t_anti_offline_whitelist` (
  `uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='反脱机挂强对抗白名单';

-- ----------------------------
-- Records of t_anti_offline_whitelist
-- ----------------------------

-- ----------------------------
-- Table structure for t_battle_pass_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_battle_pass_schedule_config`;
CREATE TABLE `t_battle_pass_schedule_config` (
  `schedule_id` int(11) NOT NULL COMMENT '排期ID, 与Excel中配置一致',
  `begin_date` date NOT NULL COMMENT '开始日期',
  `end_date` date NOT NULL COMMENT '结束日期',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='战令(BattlePass)排期配置表';

-- ----------------------------
-- Records of t_battle_pass_schedule_config
-- ----------------------------
INSERT INTO `t_battle_pass_schedule_config` VALUES (1100, '2024-01-01', '2024-01-31');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1200, '2024-02-01', '2024-03-03');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1300, '2024-03-04', '2024-04-04');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1400, '2024-04-05', '2024-05-06');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1500, '2024-05-07', '2024-06-07');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1600, '2024-06-08', '2024-07-09');

-- ----------------------------
-- Table structure for t_chat_block_config
-- ----------------------------
DROP TABLE IF EXISTS `t_chat_block_config`;
CREATE TABLE `t_chat_block_config` (
  `uid` int(10) unsigned NOT NULL,
  `end_time` datetime NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家禁言配置';

-- ----------------------------
-- Records of t_chat_block_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_client_watchdog_uid_list_config
-- ----------------------------
DROP TABLE IF EXISTS `t_client_watchdog_uid_list_config`;
CREATE TABLE `t_client_watchdog_uid_list_config` (
  `uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='watchdog开启名单';

-- ----------------------------
-- Records of t_client_watchdog_uid_list_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_cmd_frequency_config
-- ----------------------------
DROP TABLE IF EXISTS `t_cmd_frequency_config`;
CREATE TABLE `t_cmd_frequency_config` (
  `cmd_id` int(7) unsigned NOT NULL,
  `frequency_limit` float NOT NULL COMMENT '单位时间内最大收包量',
  `discard_packet_freq_limit` float NOT NULL COMMENT '超过此频率时丢弃本次协议包',
  `disconnect_freq_limit` float NOT NULL COMMENT '超过此频率时踢玩家下线'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='协议频率限制配置';

-- ----------------------------
-- Records of t_cmd_frequency_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_cmd_str_frequency_config
-- ----------------------------
DROP TABLE IF EXISTS `t_cmd_str_frequency_config`;
CREATE TABLE `t_cmd_str_frequency_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cmd_str` varchar(200) NOT NULL COMMENT '通信包名',
  `frequency_limit` float NOT NULL COMMENT '单位时间内最大收包量',
  `discard_packet_freq_limit` float NOT NULL COMMENT '超过此频率时丢弃本次协议包',
  `disconnect_freq_limit` float NOT NULL COMMENT '超过此频率时踢玩家下线',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='协议频率限制配置';

-- ----------------------------
-- Records of t_cmd_str_frequency_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_feature_block_config
-- ----------------------------
DROP TABLE IF EXISTS `t_feature_block_config`;
CREATE TABLE `t_feature_block_config` (
  `uid` int(10) unsigned NOT NULL,
  `type` int(11) unsigned NOT NULL,
  `end_time` datetime NOT NULL,
  `begin_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`uid`,`type`) USING BTREE,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='玩家玩法封禁配置';

-- ----------------------------
-- Records of t_feature_block_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_feature_switch_off_config
-- ----------------------------
DROP TABLE IF EXISTS `t_feature_switch_off_config`;
CREATE TABLE `t_feature_switch_off_config` (
  `id` int(11) unsigned NOT NULL,
  `type` int(11) unsigned NOT NULL,
  `msg` varchar(1000) NOT NULL DEFAULT '',
  `desc` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='关闭系统开关表';

-- ----------------------------
-- Records of t_feature_switch_off_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_gacha_newbie_url_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gacha_newbie_url_config`;
CREATE TABLE `t_gacha_newbie_url_config` (
  `priority` int(11) unsigned NOT NULL COMMENT '优先级',
  `gacha_prob_url` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋概率展示url',
  `gacha_record_url` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋记录url',
  PRIMARY KEY (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='新手扭蛋url配置';

INSERT INTO `t_gacha_newbie_url_config` (`priority`, `gacha_prob_url`, `gacha_record_url`) VALUES
(9999,  CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=16'), CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=100'));

-- ----------------------------
-- Records of t_gacha_newbie_url_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_gacha_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gacha_schedule_config`;
CREATE TABLE `t_gacha_schedule_config` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `gacha_type` int(11) NOT NULL DEFAULT 0 COMMENT '扭蛋类型',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `cost_item_id` int(11) unsigned NOT NULL COMMENT '消耗材料ID',
  `cost_item_num` int(11) unsigned NOT NULL COMMENT '消耗材料数量',
  `gacha_pool_id` int(11) unsigned NOT NULL COMMENT 'Gacha根ID',
  `gacha_prob_rule_id` int(11) unsigned NOT NULL COMMENT 'Gacha概率配置ID',
  `gacha_up_config` varchar(512) NOT NULL DEFAULT '' COMMENT 'UP配置',
  `gacha_rule_config` varchar(512) NOT NULL DEFAULT '' COMMENT '保底规则配置',
  `gacha_prefab_path` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋Prefab路径',
  `gacha_preview_prefab_path` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋预览Prefab路径',
  `gacha_prob_url` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋概率展示url',
  `gacha_record_url` varchar(512) NOT NULL DEFAULT '' COMMENT '扭蛋记录url',
  `gacha_prob_url_oversea` varchar(512) NOT NULL DEFAULT '' COMMENT '海外扭蛋概率展示url',
  `gacha_record_url_oversea` varchar(512) NOT NULL DEFAULT '' COMMENT '海外扭蛋记录url',
  `gacha_sort_id` int(11) unsigned NOT NULL COMMENT '扭蛋排序权重',
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0不生效，1生效',
  `title_textmap` varchar(256) NOT NULL DEFAULT '' COMMENT 'Gacha显示多语言文本',
  `display_up4_item_list` varchar(512) NOT NULL DEFAULT '' COMMENT '显示up4星物品',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='扭蛋活动配置';

-- ----------------------------
-- Records of t_gacha_schedule_config
-- ----------------------------
INSERT INTO `t_gacha_schedule_config` VALUES (
    931,
    202,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    223,
    1,
    601,
    11,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[1029]}]}',
    '{}',
    'GachaShowPanel_A018',
    'UI_Tab_GachaShowPanel_A018',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=931'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=202'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=931'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=202'),
    1006,
    1,
    'UI_GACHA_SHOW_PANEL_A018_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    932,
    400,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    223,
    1,
    401,
    11,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[1026]}]}',
    '{}',
    'GachaShowPanel_A031',
    'UI_Tab_GachaShowPanel_A031',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=932'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=400'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=932'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=400'),
    1005,
    1,
    'UI_GACHA_SHOW_PANEL_A031_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    933,
    201,
    '2022-11-03 13:37:46',
    '2099-11-30 13:37:55',
    223,
    1,
    501,
    11,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[1057]}]}',
    '{}',
    'GachaShowPanel_A061',
    'UI_Tab_GachaShowPanel_A061',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=933'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=201'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=933'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=201'),
    1004,
    1,
    'UI_GACHA_SHOW_PANEL_A061_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    934,
    301,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    223,
    1,
    301,
    11,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[1069]}]}',
    '{}',
    'GachaShowPanel_A091',
    'UI_Tab_GachaShowPanel_A091',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=934'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=301'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=934'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=301'),
    1003,
    1,
    'UI_GACHA_SHOW_PANEL_A0103_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    935,
    200,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    224,
    1,
    201,
    11,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":25,"item_list":[1002,1003,1016,1022,1026,1029,1030,1033,1035,1037,1038,1041,1042,1046,1047,1049,1051,1052,1054,1057,1058,1063,1066]}]}',
    '{}',
    'GachaShowPanel_A022',
    'UI_Tab_GachaShowPanel_A022',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=935'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=200'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=935'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=200'),
    1002,
    1,
    'UI_GACHA_SHOW_PANEL_A022_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    936,
    302,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    224,
    1,
    701,
    12,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[14511,15509]}]}',
    '{}',
    'GachaShowPanel_A105',
    'UI_Tab_GachaShowPanel_A105',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=936'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=302'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=936'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=302'),
    1001,
    1,
    'UI_GACHA_SHOW_PANEL_A021_TITLE',
    ''
);

INSERT INTO `t_gacha_schedule_config` VALUES (
    937,
    300,
    '2022-11-02 13:37:46',
    '2099-11-30 13:37:55',
    224,
    1,
    801,
    12,
    '{"gacha_up_list":[{"item_parent_type":1,"prob":2,"item_list":[12502,13501]}]}',
    '{}',
    'GachaShowPanel_A035',
    'UI_Tab_GachaShowPanel_A035',
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=937'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=300'),
    CONCAT('https://', @url_api, '/api/v2/gacha/prob?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&scheduleId=937'),
    CONCAT('https://', @url_api, '/api/v2/gacha/record?authkey_ver=1&sign_type=2&auth_appid=webview_gacha&gachaType=300'),
    1000,
    1,
    'UI_GACHA_SHOW_PANEL_A035_TITLE',
    ''
);

-- ----------------------------
-- Table structure for t_gameplay_recommendation_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gameplay_recommendation_config`;
CREATE TABLE `t_gameplay_recommendation_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '条目ID',
  `begin_time` datetime NOT NULL COMMENT '生效时间',
  `json_str` mediumtext NOT NULL COMMENT '定义为proto::GameplayRecommendationConfig',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0不生效，1生效',
  PRIMARY KEY (`id`),
  UNIQUE KEY `begin_time` (`begin_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='养成推荐数据';

-- ----------------------------
-- Records of t_gameplay_recommendation_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_h5_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_h5_activity_schedule_config`;
CREATE TABLE `t_h5_activity_schedule_config` (
  `schedule_id` int(11) NOT NULL COMMENT '排期ID',
  `activity_id` int(11) NOT NULL COMMENT '活动ID',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `content_close_time` datetime NOT NULL COMMENT '玩法结束时间',
  `prefab_path` varchar(512) NOT NULL DEFAULT '' COMMENT '活动底图文件',
  `url_cn` varchar(512) NOT NULL DEFAULT '' COMMENT '活动链接（国内）',
  `url_os` varchar(512) NOT NULL DEFAULT '' COMMENT '活动链接（海外）',
  `is_entrance_open` tinyint(4) NOT NULL DEFAULT 1 COMMENT '入口开关：0关闭，1开放',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='H5活动排期配置';

-- ----------------------------
-- Records of t_h5_activity_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_inject_fix_config
-- ----------------------------
DROP TABLE IF EXISTS `t_inject_fix_config`;
CREATE TABLE `t_inject_fix_config` (
  `config_id` int(11) unsigned NOT NULL,
  `inject_fix` blob NOT NULL COMMENT 'inject_fix',
  `uid_list` varchar(4096) NOT NULL DEFAULT '' COMMENT '白名单多个,隔开，空表示不开启白名单',
  `platform_type_list` varchar(4096) NOT NULL DEFAULT '' COMMENT '(如安卓 ios) 多个,隔开，空表示对平台不做要求',
  `percent` tinyint(3) NOT NULL DEFAULT 0 COMMENT '灰度百分比',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '创建时间',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='inject_fix 配置表';

-- ----------------------------
-- Records of t_inject_fix_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_live_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_live_schedule_config`;
CREATE TABLE `t_live_schedule_config` (
  `live_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '直播ID',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `live_url` varchar(512) NOT NULL DEFAULT '' COMMENT '直播地址',
  `spare_live_url` varchar(512) NOT NULL DEFAULT '' COMMENT '备用直播地址',
  PRIMARY KEY (`live_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Live broadcast schedule';

-- ----------------------------
-- Records of t_live_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_black_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_black_ip_config`;
CREATE TABLE `t_login_black_ip_config` (
  `ip` int(10) unsigned NOT NULL,
  `ip_str` varchar(64) NOT NULL COMMENT '对应的字符串',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Register & log in IP blacklist';

-- ----------------------------
-- Records of t_login_black_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_black_uid_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_black_uid_config`;
CREATE TABLE `t_login_black_uid_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `msg` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Login blacklist configuration';

-- ----------------------------
-- Records of t_login_black_uid_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_reward_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_reward_config`;
CREATE TABLE `t_login_reward_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_type` tinyint(4) NOT NULL DEFAULT 0,
  `reward_rules` varchar(1024) NOT NULL,
  `email_valid_days` int(11) NOT NULL,
  `email_title` varchar(200) NOT NULL,
  `email_sender` varchar(200) NOT NULL,
  `email_content` text NOT NULL,
  `item_list` varchar(1024) NOT NULL COMMENT '奖励列表，proto3的json格式',
  `effective_account_type_list` varchar(100) NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0不生效，1生效',
  `tag` varchar(200) NOT NULL DEFAULT '' COMMENT '标签',
  `importance` int(11) NOT NULL DEFAULT 0,
  `is_collectible` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0不可收藏，1可收藏',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Daily login reward configuration';

-- ----------------------------
-- Records of t_login_reward_config
-- ----------------------------
INSERT INTO `t_login_reward_config` VALUES (1001, 0, 'MATH_EXPR_LEVEL >= 2', 365, 'Hello newcomers', 'YuukiPS', 'Now you have reached level 2, if you are confused about how to use commands, please visit <type=\"browser\" text=\"Web Commands\" href=\"https://ps.yuuki.me/command\"/> because you cant do it in-game because there is no ayaka bot on the gio server.', '{\r\n  \"item_list\": [\r\n    {\r\n      \"item_id\": 201,\r\n      \"item_num\": 5000\r\n    }\r\n  ]\r\n}', '1', '2024-01-01 00:00:00', '2025-01-01 00:00:00', 1, '', 0, 0);

-- ----------------------------
-- Table structure for t_luashell_config
-- ----------------------------
DROP TABLE IF EXISTS `t_luashell_config`;
CREATE TABLE `t_luashell_config` (
  `luashell_config_id` int(11) unsigned NOT NULL,
  `lua_shell` mediumblob NOT NULL COMMENT 'lua脚本',
  `uid_list` varchar(4096) NOT NULL DEFAULT '' COMMENT '白名单多个,隔开，空表示不开启白名单',
  `platform_type_list` varchar(4096) NOT NULL DEFAULT '' COMMENT '(如安卓 ios) 多个,隔开，空表示对平台不做要求',
  `percent` tinyint(3) NOT NULL DEFAULT 0 COMMENT '灰度百分比',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '创建时间',
  `protocol_type` int(2) unsigned NOT NULL DEFAULT 0 COMMENT '协议类型',
  `use_type` int(11) unsigned NOT NULL DEFAULT 1 COMMENT '用于标识luashell的用途：1.普通luashell；2.安全库lua',
  `is_check_client_report` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否检查客户端回复上报',
  `is_kick` tinyint(1) NOT NULL DEFAULT 0 COMMENT '检查客户端回复失败后是否踢下线',
  `check_json_key` varchar(32) NOT NULL DEFAULT '' COMMENT '检查客户端回复的key的字符串',
  `channel` int(11) NOT NULL DEFAULT 0 COMMENT '下发通道',
  PRIMARY KEY (`luashell_config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='luashell configuration table';

-- ----------------------------
-- Records of t_luashell_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mail_block_tag_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mail_block_tag_config`;
CREATE TABLE `t_mail_block_tag_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Email blocking label configuration';

-- ----------------------------
-- Records of t_mail_block_tag_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mtp_blacklist_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mtp_blacklist_config`;
CREATE TABLE `t_mtp_blacklist_config` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认主键',
  `type` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '对抗类型',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='The blacklist ID that MTP wants to kick offline';

-- ----------------------------
-- Records of t_mtp_blacklist_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mtp_whitelist_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mtp_whitelist_config`;
CREATE TABLE `t_mtp_whitelist_config` (
  `id` int(11) NOT NULL COMMENT 'mtp上报id',
  `reason` varchar(512) NOT NULL COMMENT 'mtp上报id对应的reason',
  `match_type` int(11) NOT NULL COMMENT '匹配类型：1、2、3分别表示包含、开头、单一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='MTP whitelist';

-- ----------------------------
-- Records of t_mtp_whitelist_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_op_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_op_activity_schedule_config`;
CREATE TABLE `t_op_activity_schedule_config` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '排期ID',
  `config_id` int(11) NOT NULL DEFAULT 0 COMMENT '活动配置ID',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Operational activity configuration';

-- ----------------------------
-- Records of t_op_activity_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_questionnaire_mail_config
-- ----------------------------
DROP TABLE IF EXISTS `t_questionnaire_mail_config`;
CREATE TABLE `t_questionnaire_mail_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_valid_days` int(11) NOT NULL,
  `email_title` varchar(200) NOT NULL,
  `email_sender` varchar(200) NOT NULL,
  `email_content` text NOT NULL,
  `item_list` varchar(1024) NOT NULL COMMENT '奖励列表，proto3的json格式',
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0不生效，1生效',
  `tag` varchar(200) NOT NULL DEFAULT '' COMMENT '标签',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Email configuration';

-- ----------------------------
-- Records of t_questionnaire_mail_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_rebate_config
-- ----------------------------
DROP TABLE IF EXISTS `t_rebate_config`;
CREATE TABLE `t_rebate_config` (
  `account_type` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '账号类型',
  `account_uid` varchar(128) NOT NULL DEFAULT '' COMMENT '绑定的账号UID',
  `item_list` varchar(128) NOT NULL DEFAULT '' COMMENT 'List of recharge rebate items, separated by commas and then colons',
  PRIMARY KEY (`account_type`,`account_uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Recharge and rebate list';

-- ----------------------------
-- Records of t_rebate_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_red_point_config
-- ----------------------------
DROP TABLE IF EXISTS `t_red_point_config`;
CREATE TABLE `t_red_point_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '条目ID',
  `content_id` int(11) unsigned NOT NULL COMMENT '活动ID或调查问卷ID等二级ID',
  `trigger_time` datetime NOT NULL COMMENT '触发时间',
  `expire_time` datetime NOT NULL COMMENT '失效时间',
  `red_point_type` int(11) unsigned NOT NULL COMMENT '红点类型（红点ID/红点位key）',
  `is_daily_refresh` int(11) unsigned NOT NULL COMMENT '是否进行每日刷新',
  `daily_refresh_second` int(11) unsigned NOT NULL COMMENT '每天0点开始的第几秒进行每日刷新',
  `player_level` int(11) unsigned NOT NULL DEFAULT 0 COMMENT '最小玩家等级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Red dot configuration';

-- ----------------------------
-- Records of t_red_point_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_register_black_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_register_black_ip_config`;
CREATE TABLE `t_register_black_ip_config` (
  `ip` varchar(64) NOT NULL,
  `ip_desc` varchar(256) NOT NULL DEFAULT '' COMMENT 'IP地址备注信息',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Register IP blacklist';

-- ----------------------------
-- Records of t_register_black_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_security_library_config
-- ----------------------------
DROP TABLE IF EXISTS `t_security_library_config`;
CREATE TABLE `t_security_library_config` (
  `platform_type_str` varchar(64) NOT NULL DEFAULT '' COMMENT '平台类型，定义在define.proto的PlatformType',
  `version_str` varchar(64) NOT NULL DEFAULT '' COMMENT '版本号',
  `md5_list` text NOT NULL COMMENT 'md5校验值，逗号分隔',
  `is_forbid_login` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'MD5不一致时是否禁止登录',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0不生效，1生效',
  PRIMARY KEY (`platform_type_str`,`version_str`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Security library configuration';

-- ----------------------------
-- Records of t_security_library_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_sign_in_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sign_in_schedule_config`;
CREATE TABLE `t_sign_in_schedule_config` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '排期ID',
  `config_id` int(11) NOT NULL DEFAULT 0 COMMENT '签到配置ID',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Check-in activity configuration';

-- ----------------------------
-- Records of t_sign_in_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_stop_server_login_white_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_stop_server_login_white_ip_config`;
CREATE TABLE `t_stop_server_login_white_ip_config` (
  `ip` varchar(32) NOT NULL,
  `desc` varchar(32) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Secondary dispatch login whitelist when the server is stopped';

-- ----------------------------
-- Records of t_stop_server_login_white_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_textmap_config
-- ----------------------------
DROP TABLE IF EXISTS `t_textmap_config`;
CREATE TABLE `t_textmap_config` (
  `text_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'textmap的key',
  `delete_time` datetime NOT NULL COMMENT '失效时间，时间一到就会删除这条记录',
  `en` text NOT NULL COMMENT '英文',
  `sc` text NOT NULL COMMENT '简体中文',
  `tc` text NOT NULL COMMENT '繁体中文',
  `fr` text NOT NULL COMMENT '法语',
  `de` text NOT NULL COMMENT '德语',
  `es` text NOT NULL COMMENT '西班牙语',
  `pt` text NOT NULL COMMENT '葡萄牙语',
  `ru` text NOT NULL COMMENT '俄语',
  `jp` text NOT NULL COMMENT '日语',
  `kr` text NOT NULL COMMENT '韩语',
  `th` text NOT NULL COMMENT '泰文',
  `vn` text NOT NULL COMMENT '越南语',
  `id` text NOT NULL COMMENT '印尼语',
  `tr` text NOT NULL COMMENT '土耳其语',
  `it` text NOT NULL COMMENT '意大利语',
  PRIMARY KEY (`text_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Server-side textmaps are generally used for emails and need to control entries because they are all loaded into memory.';

-- ----------------------------
-- Records of t_textmap_config
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
